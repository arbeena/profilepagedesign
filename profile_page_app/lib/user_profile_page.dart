import 'package:flutter/material.dart';
import 'user_profile.dart';

class UserProfilePage extends StatelessWidget {
  final String name;
  final String profession;
  final String location;
  final String socialLink;
  final String bio;
  UserProfilePage(
      {this.name, this.profession, this.location, this.socialLink, this.bio});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Color(0xFFF6D6DF),
      appBar: AppBar(
        backgroundColor: Colors.white,
        leading: IconButton(
          onPressed: () {
            Navigator.pushNamed(context, '/');
          },
          icon: Icon(
            Icons.arrow_back_ios,
            color: Colors.pink.shade500,
          ),
        ),
        actions: <Widget>[
          Padding(
            padding: EdgeInsets.only(right: 20.0),
            child: Icon(
              Icons.bookmark_border_outlined,
              color: Colors.pink.shade500,
              size: 26.0,
            ),
          ),
          Padding(
            padding: EdgeInsets.only(right: 20.0),
            child: Icon(
              Icons.share_outlined,
              color: Colors.pink.shade500,
            ),
          ),
        ],
      ),
      body: UserProfile(
        name: name,
        profession: profession,
        location: location,
        socialLink: socialLink,
        bio: bio,
      ),
    );
  }
}
