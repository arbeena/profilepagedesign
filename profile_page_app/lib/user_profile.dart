import 'package:flutter/material.dart';
import 'package:profile_page_app/circle_highlights/circle_highlights.dart';
import 'package:profile_page_app/circle_highlights/hightlights_content_model.dart';
import 'grid_view_contents/tab_bar_layout.dart';
import 'grid_view_contents/grid_view.dart';

class UserProfile extends StatefulWidget {
  final String name;
  final String profession;
  final String location;
  final String socialLink;
  final String bio;
  UserProfile(
      {this.name, this.profession, this.location, this.socialLink, this.bio});

  @override
  _UserProfileState createState() => _UserProfileState();
}

class _UserProfileState extends State<UserProfile> {
  List<HighlightContents> highlightDetails = [
    HighlightContents(
        title: 'Top works',
        highlightImage:
            'https://images.shaadisaga.com/shaadisaga_production/photos/pictures/000/668/007/new_large/BeFunky-collage_%2817%29.jpg?1547726347'),
    HighlightContents(
        title: 'Wedding',
        highlightImage:
            'https://i.pinimg.com/originals/bd/c9/dd/bdc9dd4ab5ab0dd67a815e4287f36186.png'),
    HighlightContents(
        title: 'Pre wedding',
        highlightImage:
            'https://de927adv5b23k.cloudfront.net/wp-content/uploads/2017/12/03191752/What-to-wear-to-a-pre-wedding-shoot-for-dramatic-effects.jpg'),
    HighlightContents(
        title: 'Haldi',
        highlightImage:
            'https://i.pinimg.com/474x/eb/6f/49/eb6f491e905728c228b762b51a06d1b1.jpg'),
    HighlightContents(
        title: 'Mehendi',
        highlightImage:
            'https://cdn.lifestyleasia.com/wp-content/uploads/sites/7/2020/06/10211005/101725115_957150821448117_539824906193807031_n-819x1024.jpg'),
    HighlightContents(
        title: 'Reception',
        highlightImage:
            'https://images.shaadisaga.com/shaadisaga_production/photos/pictures/000/642/865/new_medium/43778424_327941688008001_7761544586396714060_n.jpg?1546003728'),
    HighlightContents(
        title: 'Party',
        highlightImage:
            'https://i.pinimg.com/474x/6d/57/47/6d5747ec474bfe38b5e2c97c675e5bca.jpg'),
    HighlightContents(
        title: 'Sangeet',
        highlightImage:
            'https://shaadiwish.com/blog/wp-content/uploads/2020/06/sangeet-bridal-look.jpg'),
  ];

  @override
  Widget build(BuildContext context) {
    return SingleChildScrollView(
      child: Container(
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Row(
              children: [
                Container(
                  margin: EdgeInsets.only(left: 15.0, top: 15.0),
                  child: CircleAvatar(
                    radius: 55.0,
                    backgroundColor: Colors.white,
                    child: CircleAvatar(
                      radius: 50.0,
                      backgroundImage: NetworkImage(
                          'https://media.weddingz.in/images/8c611b73f2b33a8108871c8ac6ffc61d/top-5-indian-bridal-make-up-trends-2017.jpg'),
                    ),
                  ),
                ),
                SizedBox(
                  width: 15.0,
                ),
                Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Text(
                      widget.name,
                      style: TextStyle(
                          color: Colors.black,
                          fontSize: 22.0,
                          fontWeight: FontWeight.bold,
                          fontFamily: 'Times New Roman'),
                    ),
                    SizedBox(
                      height: 10.0,
                    ),
                    Row(
                      children: [
                        Icon(
                          Icons.camera_alt,
                          size: 16.0,
                          color: Colors.pink.shade500,
                        ),
                        SizedBox(
                          width: 6,
                        ),
                        Text(
                          widget.profession,
                          style: TextStyle(color: Colors.black, fontSize: 14.0),
                        ),
                        SizedBox(
                          width: 12.0,
                        ),
                        Icon(
                          Icons.place,
                          size: 16.0,
                          color: Colors.pink.shade500,
                        ),
                        SizedBox(
                          width: 6,
                        ),
                        Text(
                          widget.location,
                          style: TextStyle(color: Colors.black, fontSize: 14.0),
                        ),
                      ],
                    ),
                    SizedBox(
                      height: 10.0,
                    ),
                    Row(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Text(
                          '1459',
                          style: TextStyle(
                            color: Colors.pink.shade500,
                            fontSize: 18.0,
                            fontWeight: FontWeight.w700,
                          ),
                        ),
                        SizedBox(
                          width: 22.0,
                        ),
                        Text(
                          '234K',
                          style: TextStyle(
                            color: Colors.pink.shade500,
                            fontSize: 18.0,
                            fontWeight: FontWeight.w700,
                          ),
                        ),
                        SizedBox(
                          width: 22.0,
                        ),
                        Text(
                          '1234',
                          style: TextStyle(
                            color: Colors.pink.shade500,
                            fontSize: 18.0,
                            fontWeight: FontWeight.w700,
                          ),
                        ),
                      ],
                    ),
                    SizedBox(
                      height: 2.0,
                    ),
                    Row(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Text(
                          'Videos',
                          style: TextStyle(
                            color: Colors.pink.shade500,
                            fontSize: 10.0,
                            fontWeight: FontWeight.w700,
                          ),
                        ),
                        SizedBox(
                          width: 26.0,
                        ),
                        Text(
                          'Followers',
                          style: TextStyle(
                            color: Colors.pink.shade500,
                            fontSize: 10.0,
                            fontWeight: FontWeight.w700,
                          ),
                        ),
                        SizedBox(
                          width: 26.0,
                        ),
                        Text(
                          'Following',
                          style: TextStyle(
                            color: Colors.pink.shade500,
                            fontSize: 10.0,
                            fontWeight: FontWeight.w700,
                          ),
                        ),
                      ],
                    ),
                  ],
                ),
              ],
            ),
            Container(
              margin: EdgeInsets.symmetric(vertical: 20.0, horizontal: 15.0),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Text(
                    widget.bio,
                    style: TextStyle(
                      fontSize: 12.0,
                      color: Colors.black,
                      fontWeight: FontWeight.w500,
                      letterSpacing: 0.5,
                      height: 1.6,
                    ),
                  ),
                  SizedBox(
                    height: 8.0,
                  ),
                  Text(
                    widget.socialLink,
                    textAlign: TextAlign.start,
                    style: TextStyle(
                      color: Colors.pink.shade500,
                      fontWeight: FontWeight.w900,
                      fontSize: 14.0,
                    ),
                  ),
                ],
              ),
            ),
            Container(
              margin: EdgeInsets.all(10.0),
              height: 120.0,
              child: ListView.builder(
                scrollDirection: Axis.horizontal,
                itemCount: highlightDetails.length,
                itemBuilder: (context, index) {
                  return CircleHighlights(
                    highlights: highlightDetails,
                    index: index,
                  );
                },
              ),
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
              children: [
                RaisedButton(
                  padding:
                      EdgeInsets.symmetric(vertical: 12.0, horizontal: 50.0),
                  shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(18.0),
                      side: BorderSide(color: Colors.pink.shade500)),
                  onPressed: () {},
                  color: Colors.pink.shade500,
                  textColor: Colors.white,
                  child: Text('Follow', style: TextStyle(fontSize: 14)),
                ),
                SizedBox(
                  width: 10.0,
                ),
                RaisedButton(
                  padding:
                      EdgeInsets.symmetric(vertical: 12.0, horizontal: 50.0),
                  shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(18.0),
                      side: BorderSide(color: Colors.red)),
                  onPressed: () {},
                  color: Colors.pink.shade500,
                  textColor: Colors.pink.shade500,
                  child: Text(
                    'Ask ' + widget.name,
                    style: TextStyle(color: Colors.white),
                  ),
                ),
              ],
            ),
            SizedBox(
              height: 18.0,
            ),
            Container(
              height: MediaQuery.of(context).size.height,
              width: MediaQuery.of(context).size.width,
              child: DefaultTabController(
                length: 3,
                child: Scaffold(
                  appBar: AppBar(
                    automaticallyImplyLeading: false,
                    backgroundColor: Colors.white,
                    title: TabLayout(),
                  ),
                  body: GridViewContents(),
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
