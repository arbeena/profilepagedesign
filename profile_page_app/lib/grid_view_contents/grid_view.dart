import 'package:flutter/material.dart';
import 'package:profile_page_app/grid_view_contents/grid_image.dart';

class GridViewContents extends StatefulWidget {
  @override
  _GridViewContentsState createState() => _GridViewContentsState();
}

class _GridViewContentsState extends State<GridViewContents> {
  List<String> gridImages = [
    'https://www.shaadidukaan.com/vogue/wp-content/uploads/2019/05/indian-beauty-makeup.jpeg',
    'https://static.toiimg.com/imagenext/toiblogs/photo/readersblog/wp-content/uploads/2020/04/Indian-Bridal-Jewellery.jpg',
    'https://i.pinimg.com/474x/eb/6f/49/eb6f491e905728c228b762b51a06d1b1.jpg',
    'https://i.pinimg.com/originals/48/ed/35/48ed35cbc46d03299c0a9ed0c7409b3e.jpg',
    'https://images.shaadisaga.com/shaadisaga_production/photos/pictures/000/642/865/new_medium/43778424_327941688008001_7761544586396714060_n.jpg?1546003728',
    'https://shaadiwish.com/blog/wp-content/uploads/2020/06/sangeet-bridal-look.jpg',
    'https://i.pinimg.com/474x/6d/57/47/6d5747ec474bfe38b5e2c97c675e5bca.jpg',
    'https://image.wedmegood.com/resized-nw/700X/wp-content/uploads/2018/11/126.jpg',
    'https://images.shaadisaga.com/shaadisaga_production/photos/pictures/001/801/869/new_medium/wow_2.jpg?1592287596',
    'https://images.shaadisaga.com/shaadisaga_production/photos/pictures/000/668/007/new_large/BeFunky-collage_%2817%29.jpg?1547726347',
    'https://de927adv5b23k.cloudfront.net/wp-content/uploads/2017/12/03191752/What-to-wear-to-a-pre-wedding-shoot-for-dramatic-effects.jpg',
    'https://i.pinimg.com/originals/bd/c9/dd/bdc9dd4ab5ab0dd67a815e4287f36186.png',
    'https://www.zowed.com/blog/wp-content/uploads/2018/11/WhatsApp-Image-2019-01-28-at-16.07.01.jpeg',
    'http://www.studiopepphotography.com/images/pre_wedding/banner.jpg',
    'https://cdn.lifestyleasia.com/wp-content/uploads/sites/7/2020/06/10211005/101725115_957150821448117_539824906193807031_n-819x1024.jpg',
  ];

  @override
  Widget build(BuildContext context) {
    return TabBarView(
      children: [
        GridViewBuilder(gridImages: gridImages),
        GridViewBuilder(gridImages: gridImages),
        GridViewBuilder(gridImages: gridImages),
      ],
    );
  }
}

class GridViewBuilder extends StatelessWidget {
  const GridViewBuilder({
    @required this.gridImages,
  });

  final List<String> gridImages;

  @override
  Widget build(BuildContext context) {
    return GridView.builder(
      itemCount: gridImages.length,
      physics: ScrollPhysics(),
      gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
        crossAxisCount: 3,
      ),
      itemBuilder: (BuildContext context, int index) {
        return Container(
          margin: EdgeInsets.symmetric(horizontal: 2.0, vertical: 2.0),
          child: GridTile(
            child: GestureDetector(
              child: Image.network(
                gridImages[index],
                fit: BoxFit.cover,
              ),
              onTap: () {
                Navigator.push(
                  context,
                  MaterialPageRoute(
                    builder: (context) => GridImage(
                      image: gridImages[index],
                    ),
                  ),
                );
              },
            ),
          ),
        );
      },
    );
  }
}
