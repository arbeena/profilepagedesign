import 'package:flutter/material.dart';

class GridImage extends StatefulWidget {
  final String image;
  GridImage({this.image});

  @override
  _GridImageState createState() => _GridImageState();
}

class _GridImageState extends State<GridImage> {
  @override
  Widget build(BuildContext context) {
    return Container(
      height: MediaQuery.of(context).size.height,
      width: MediaQuery.of(context).size.width,
      child: Image.network(widget.image),
    );
  }
}
