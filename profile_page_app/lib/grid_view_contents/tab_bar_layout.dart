import 'package:flutter/material.dart';

class TabLayout extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return TabBar(
      tabs: [
        Tab(
          child: Text(
            'Photos',
            style: TextStyle(
              color: Colors.black,
              fontWeight: FontWeight.bold,
              fontSize: 14.0,
            ),
          ),
        ),
        Tab(
          child: Text(
            'Videos',
            style: TextStyle(
              color: Colors.black,
              fontWeight: FontWeight.bold,
              fontSize: 14.0,
            ),
          ),
        ),
        Tab(
          child: Text(
            'Albums',
            style: TextStyle(
              color: Colors.black,
              fontWeight: FontWeight.bold,
              fontSize: 14.0,
            ),
          ),
        ),
      ],
      indicator: UnderlineTabIndicator(
        insets: EdgeInsets.symmetric(vertical: 8.0, horizontal: 5.0),
        borderSide: BorderSide(
          width: 2.0,
          color: Colors.pink.shade700,
        ),
      ),
      indicatorSize: TabBarIndicatorSize.label,
    );
  }
}
