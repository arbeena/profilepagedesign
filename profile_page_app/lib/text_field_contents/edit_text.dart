import 'package:flutter/material.dart';

class EditText extends StatelessWidget {
  final String hintName;
  final Function validator;
  final Function onSaved;

  const EditText({
    @required this.hintName,
    this.validator,
    this.onSaved,
  });

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: EdgeInsets.symmetric(vertical: 8.0, horizontal: 24.0),
      child: TextFormField(
        validator: validator,
        onSaved: onSaved,
        decoration: InputDecoration(
          focusColor: Colors.pink.shade500,
          fillColor: Color(0xFF350250),
          hintText: hintName,
          hintStyle: TextStyle(
            color: Color(0xFF626063),
            fontSize: 14,
          ),
          labelText: hintName,
          labelStyle: TextStyle(
              fontSize: 15,
              color: Colors.purple.shade900,
              fontWeight: FontWeight.bold),
        ),
      ),
    );
  }
}
