import 'package:flutter/material.dart';

class EditTextIcon extends StatelessWidget {
  final String editName;
  final IconData iconData;
  final Function validator;
  final Function onSaved;
  const EditTextIcon(
      {@required this.editName, this.iconData, this.validator, this.onSaved});

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: EdgeInsets.symmetric(vertical: 8.0, horizontal: 24.0),
      child: TextFormField(
        validator: validator,
        onSaved: onSaved,
        decoration: InputDecoration(
          suffixIcon: Icon(iconData),
          fillColor: Color(0xFF350250),
          focusColor: Colors.pink.shade500,
          hintText: editName,
          hintStyle: TextStyle(
            color: Color(0xFF626063),
            fontSize: 14,
          ),
          labelText: editName,
          labelStyle: TextStyle(
              fontSize: 15,
              color: Colors.purple.shade900,
              fontWeight: FontWeight.bold),
        ),
      ),
    );
  }
}
