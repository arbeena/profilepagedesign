import 'package:flutter/material.dart';
import 'package:profile_page_app/circle_highlights/hightlights_content_model.dart';

class CircleHighlights extends StatelessWidget {
  const CircleHighlights({@required this.highlights, this.index});

  final List<HighlightContents> highlights;
  final int index;

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        Container(
          margin: EdgeInsets.only(right: 10.0),
          child: CircleAvatar(
            radius: 42.0,
            backgroundColor: Colors.pink.shade700,
            child: CircleAvatar(
              radius: 39.0,
              backgroundColor: Colors.white,
              child: CircleAvatar(
                backgroundImage: NetworkImage(highlights[index].highlightImage),
                radius: 35.0,
              ),
            ),
          ),
        ),
        SizedBox(
          height: 10.0,
        ),
        Text(
          highlights[index].title,
          style: TextStyle(
            color: Colors.pink.shade500,
            fontWeight: FontWeight.w900,
            fontSize: 14.0,
          ),
        ),
      ],
    );
  }
}
