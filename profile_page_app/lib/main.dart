import 'package:flutter/material.dart';
import 'package:profile_page_app/grid_view_contents/grid_image.dart';
import 'package:profile_page_app/user_profile_page.dart';
import 'profile_page.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      initialRoute: '/',
      routes: {
        '/': (context) => ProfilePage(),
        '/user_profile': (context) => UserProfilePage(),
        '/grid_image': (context) => GridImage(),
      },
    );
  }
}
