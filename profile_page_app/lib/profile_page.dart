import 'package:flutter/material.dart';
import 'package:profile_page_app/user_profile_page.dart';
import 'text_field_contents/radio_button.dart';
import 'text_field_contents/edit_text.dart';
import 'text_field_contents/edit_text_with_icon.dart';

class ProfilePage extends StatefulWidget {
  @override
  _ProfilePageState createState() => _ProfilePageState();
}

class _ProfilePageState extends State<ProfilePage> {
  var formKey = GlobalKey<FormState>();
  bool autoValidate = false;
  Map map;
  String name;
  String email;
  String mobile;
  String location;
  String profession;
  String bio;
  String socialLinks;

  void validateInputs() {
    if (formKey.currentState.validate()) {
      formKey.currentState.save();
    } else {
      setState(() {
        autoValidate = true;
      });
    }
  }

  String validateMobile(String value) {
    if (value.length != 10)
      return 'Mobile Number must be of 10 digit';
    else
      return null;
  }

  String validateEmail(String value) {
    Pattern pattern =
        r'^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$';
    RegExp regex = new RegExp(pattern);
    if (!regex.hasMatch(value))
      return 'Enter Valid Email Address';
    else
      return null;
  }

  String validateTextFields(String text) {
    if (text == null || text.isEmpty)
      return 'This is a required field cant leave it empty';
    else
      return null;
  }

  void submitForm() {
    final isValid = formKey.currentState.validate();

    if (!isValid) {
      return;
    }

    Navigator.push(
      context,
      MaterialPageRoute(
          builder: (context) => UserProfilePage(
                name: name,
                profession: profession,
                location: location,
                socialLink: socialLinks,
                bio: bio,
              )),
    );

    formKey.currentState.save();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Color(0xFFDDE3FA),
      appBar: AppBar(
        backgroundColor: Colors.white,
        leading: Icon(
          Icons.arrow_back_ios,
          color: Colors.pink.shade500,
        ),
        centerTitle: true,
        title: Text(
          'UPDATE PROFILE',
          style: TextStyle(
              color: Colors.pink.shade500,
              fontSize: 18.0,
              fontWeight: FontWeight.bold),
        ),
      ),
      body: SingleChildScrollView(
        child: Container(
          margin: EdgeInsets.only(top: 20.0),
          child: Form(
            key: formKey,
            // ignore: deprecated_member_use
            autovalidate: autoValidate,
            child: Column(
              children: [
                Center(
                  child: CircleAvatar(
                    maxRadius: 50,
                    backgroundImage: AssetImage('images/profile_icon.png'),
                  ),
                ),
                EditText(
                  hintName: 'Full Name',
                  validator: validateTextFields,
                  onSaved: (String val) {
                    name = val;
                  },
                ),
                Container(
                  margin: EdgeInsets.symmetric(vertical: 5.0, horizontal: 24.0),
                  child: TextFormField(
                    keyboardType: TextInputType.emailAddress,
                    onFieldSubmitted: (value) {},
                    validator: validateEmail,
                    onSaved: (String val) {
                      email = val;
                    },
                    decoration: InputDecoration(
                      fillColor: Color(0xFF350250),
                      hintText: 'Email',
                      hintStyle: TextStyle(
                        color: Color(0xFF626063),
                        fontSize: 14,
                      ),
                      labelText: 'Email',
                      labelStyle: TextStyle(
                          fontSize: 15,
                          color: Colors.purple.shade900,
                          fontWeight: FontWeight.bold),
                    ),
                  ),
                ),
                Container(
                  margin: EdgeInsets.symmetric(vertical: 5.0, horizontal: 24.0),
                  child: TextFormField(
                    onFieldSubmitted: (value) {},
                    validator: validateMobile,
                    onSaved: (String val) {
                      mobile = val;
                    },
                    keyboardType: TextInputType.phone,
                    decoration: InputDecoration(
                      fillColor: Color(0xFF350250),
                      hintText: 'Mobile',
                      hintStyle: TextStyle(
                        color: Color(0xFF626063),
                        fontSize: 14,
                      ),
                      labelText: 'Mobile',
                      labelStyle: TextStyle(
                          fontSize: 15,
                          color: Colors.purple.shade900,
                          fontWeight: FontWeight.bold),
                    ),
                  ),
                ),
                EditTextIcon(
                  editName: 'Location',
                  iconData: Icons.place,
                  validator: validateTextFields,
                  onSaved: (String val) {
                    location = val;
                  },
                ),
                EditTextIcon(
                  editName: 'Profession',
                  iconData: Icons.account_circle_rounded,
                  validator: validateTextFields,
                  onSaved: (String val) {
                    profession = val;
                  },
                ),
                EditText(
                  hintName: 'Social Link',
                  validator: validateTextFields,
                  onSaved: (String val) {
                    socialLinks = val;
                  },
                ),
                Container(
                  child: RadioButton(),
                ),
                Container(
                  child: Padding(
                    padding: const EdgeInsets.all(15.0),
                    child: TextFormField(
                      minLines: 5,
                      maxLines: 5,
                      autocorrect: false,
                      validator: validateTextFields,
                      onSaved: (String val) {
                        bio = val;
                      },
                      decoration: InputDecoration(
                        hintText: 'Write a brief bio',
                        filled: true,
                        fillColor: Colors.white,
                        enabledBorder: OutlineInputBorder(
                          borderRadius: BorderRadius.all(Radius.circular(10.0)),
                          borderSide: BorderSide(color: Colors.grey),
                        ),
                        focusedBorder: OutlineInputBorder(
                          borderRadius: BorderRadius.all(Radius.circular(10.0)),
                          borderSide: BorderSide(color: Colors.grey),
                        ),
                      ),
                    ),
                  ),
                ),
                RaisedButton(
                  elevation: 5.0,
                  shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(14.0),
                  ),
                  color: Colors.pink.shade700,
                  padding:
                      EdgeInsets.symmetric(vertical: 14.0, horizontal: 140.0),
                  onPressed: () {
                    submitForm();
                    // Navigator.pushNamed(context, '/user_profile');
                  },
                  child: Text(
                    'Done',
                    style: TextStyle(
                      color: Colors.white,
                      fontSize: 16.0,
                    ),
                  ),
                )
              ],
            ),
          ),
        ),
      ),
    );
  }
}
